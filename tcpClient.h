#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <tuple>

class tcpClient {
public:
    int ulozitNaServer(int sockfd, int velkostRiadku, int velkostStlpca, char ** pole);
    std::tuple<int,int,char**> nacitatZoServera(int sockfd);
    int vytvoritSpojenie(const char *nazov, int cislo);
    void ukoncitSpojenie();

private:
    int sckt;
};
