#include "tcpClient.h"

int tcpClient::vytvoritSpojenie(const char *nazov, int cislo) {
    int sockfd, n;
    struct sockaddr_in serv_addr;
    struct hostent* server;

    char buffer[256];

    server = gethostbyname(nazov);
    if (server == NULL)
    {
        fprintf(stderr, "Chyba, adresa neexistuje\n");
        return 1;
    }

    bzero((char*)&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy(
            (char*)server->h_addr,
            (char*)&serv_addr.sin_addr.s_addr,
            server->h_length
    );
    serv_addr.sin_port = htons(cislo);

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("Chyba pri vytvarani socketu\n");
        return 2;
    }

    if(connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
    {
        perror("Chyba pripajania k socketu\n");
        return 3;
    }

    //this->sckt=sockfd;
    return sockfd;

}

int tcpClient::ulozitNaServer(int sockfd, int velkostRiadku, int velkostStlpca, char ** pole) {

    char buffer[256];
    buffer[0]='s';
    write(sockfd, buffer, 2);
    bzero(buffer,256);
    int len=sprintf(buffer,"%d",velkostRiadku);

    write(sockfd, buffer, len);
    bzero(buffer,256);

    len=sprintf(buffer,"%d",velkostStlpca);

    write(sockfd, buffer, len);
    bzero(buffer,256);

    for (int i = 0;i<velkostStlpca; i++)
        for (int j=0;j<velkostRiadku; j++)
            buffer[i*velkostRiadku+j]=pole[i][j];

    write(sockfd, buffer, strlen(buffer));
    bzero(buffer,256);
    return 0;
}


void tcpClient::ukoncitSpojenie() {
    close(this->sckt);
}

std::tuple<int,int,char**> tcpClient::nacitatZoServera(int sockfd) {
    int velkostRiadku,velkostStlpca;
    char **pole;
    char buffer[256];
    buffer[0]='l';
    write(sockfd, buffer, strlen(buffer));
    bzero(buffer,256);
    read(sockfd, buffer, 255);
    velkostRiadku=atoi(buffer);
    bzero(buffer,256);
    read(sockfd, buffer, 255);
    velkostStlpca=atoi(buffer);
    bzero(buffer,256);
    read(sockfd, buffer, 255);
    pole = new char*[velkostRiadku];
    for (int riadok = 0; riadok < velkostRiadku; riadok++)
        pole[riadok] = new char[velkostStlpca];


    for (int i = 0;i<velkostStlpca; i++)
        for (int j=0;j<velkostRiadku; j++)
            pole[i][j]=buffer[i*velkostRiadku+j];
    return {velkostRiadku,velkostStlpca,pole};
}


