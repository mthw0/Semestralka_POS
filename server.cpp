#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>

void *connection_handler(void *);
FILE *subor;
pthread_mutex_t *file_mutex;

int main(int argc , char *argv[])
{
    int socket_desc , client_sock , c;
    struct sockaddr_in server , client;

    socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_desc == -1) printf("Nepodarilo sa vytvorit socket\n");

    printf("Socket vytvoreny\n");

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( 5001 );

    if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0) {
        perror("Spojenie sa nepodarilo\n");
        return 1;
    }
    printf("spojenie vytvorene\n");

    listen(socket_desc , 3);

    printf("Caka sa na prichadzajuce spojenia...");
    c = sizeof(struct sockaddr_in);

    pthread_t thread_id;

    pthread_mutex_t file_mutex;
    pthread_mutex_init(&file_mutex, NULL);

    while( (client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c)) )
    {
        printf("Spojenie prijate\n");

        if( pthread_create( &thread_id , NULL ,  connection_handler , (void*) &client_sock) < 0)
        {
            perror("could not create thread");
            return 1;
        }

        printf("Obsluha vytvorena\n");
    }

    if (client_sock < 0)
    {
        perror("accept failed");
        return 1;
    }
    pthread_mutex_destroy(&file_mutex);
    return 0;
}

void *connection_handler(void *socket_desc) {

    int sock = *(int*)socket_desc;
    int read_size;
    char *message , client_message[2500];

    while( (read_size = recv(sock , client_message , 2500 , 0)) > 0 )
    {
        char pole[2500];
        if (client_message[0]=='s') {
            pthread_mutex_lock(file_mutex);
            int velkostRiadku,velkostStlpca;

            bzero(client_message,2500);

            recv(sock, client_message,2500,0);
            velkostRiadku=atoi(client_message);
            bzero(client_message,2500);

            recv(sock,client_message,2500,0);
            velkostStlpca=atoi(client_message);
            bzero(client_message,2500);

            recv(sock,client_message,2500,0); //prijatie

            printf("Ulozenie do suboru\n");

            subor = fopen("save.txt","w");

            fprintf(subor,"%d ",velkostRiadku);
            fprintf(subor,"%d ",velkostStlpca);

            for (int i=0;i<2500;i++)
                fprintf(subor,"%c",client_message[i]);

            fclose(subor);
            printf("Ukladanie prebehlo uspesne\n");
            pthread_mutex_unlock(file_mutex);

        }

        if (client_message[0]=='l'){
            subor = fopen("save.txt","r");

            int velkostRiadku,velkostStlpca,len;
            bzero(client_message,2500);
            fscanf(subor,"%d",&velkostRiadku);
            len=sprintf(client_message,"%d",velkostRiadku);
            write(sock, client_message, len);
            bzero(client_message,2500);
            fscanf(subor,"%d",&velkostStlpca);
            len=sprintf(client_message,"%d",velkostStlpca);
            write(sock, client_message, len);
            bzero(client_message,2500);
            char ** pole = new char*[velkostRiadku];
            for (int riadok = 0; riadok < velkostRiadku; riadok++)
                pole[riadok] = new char[velkostStlpca];


            for (int i = 0;i<velkostStlpca ; i++)
                for (int j=0;j<velkostRiadku;j++){
                    fscanf(subor,"%c",&pole[i][j]);

                }

            fclose(subor);

            for (int i = 0;i<velkostStlpca; i++)
                for (int j=0;j<velkostRiadku; j++)
                    client_message[i*velkostRiadku+j]=pole[i][j];

            write(sock,client_message,2500);
            bzero(client_message,2500);
        }

    }

    if(read_size == 0) printf("Klient sa odpojil\n");
    else if(read_size == -1) perror("prijatie zlyhalo");

    return 0;
}

