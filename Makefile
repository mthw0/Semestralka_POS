CC = g++
CASTI = menu.cpp mravec.cpp mravecMain.cpp mravecVypis.cpp plocha.cpp tcpClient.cpp
PREPINACE = -std=c++17

all: server mravec

server: server.cpp
	$(CC) $@.cpp -o $@ -pthread $(PREPINACE)

mravec: $(CASTI)
	$(CC) $(CASTI) -o $@ $(PREPINACE)

clean: 
	rm -f mravec server
